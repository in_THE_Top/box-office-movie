import React from "react";
import { Routes, Route } from "react-router-dom";
import { routes } from "./router";
import "./App.css";
export default function App() {
  return (
    <div>
      <div className="center">
        <Routes>
          {routes.map((item) => (
            <Route
              path={item.path}
              key={item.path}
              element={<item.element menu={item.children} />}
            >
              {item.children &&
                item.children.map((ite) => (
                  <Route
                    key={ite.path}
                    path={ite.path}
                    element={<ite.element />}
                  ></Route>
                ))}
            </Route>
          ))}
        </Routes>
      </div>
    </div>
  );
}
