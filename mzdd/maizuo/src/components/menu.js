import React from 'react'
import { routes } from "../router";
import { NavLink } from "react-router-dom";
export default function menu() {
  return (
    <div>
        <ul className="bottom">
        {routes
          .filter((item) => !item.hidden)
          .map((item) => (
            <li key={item.path}>
              <NavLink to={item.path}>
                <div>
                  <item.icon />
                </div>
                {item.til}
              </NavLink>
            </li>
          ))}
      </ul>
    </div>
  )
}
