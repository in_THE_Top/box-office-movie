import React, { useState, useEffect } from "react";
import { getShopList } from '../api'
import { List } from 'antd-mobile'
import { NavLink } from "react-router-dom";
import axios from "axios";
import { URL, inter } from "../../../api";
export default function Index() {

    const [ShopList, setShopList] = useState([]);
    const getShop = async () => {
        let res = await getShopList();
        setShopList(res.data);
    };
    const del = (id: number) => {
        axios.delete(URL + inter + 'filmsShop/' + id).then((res) => {
            getShop();
        });
    };
    useEffect(() => {
        getShop()
    }, []);
    return (
        <div>
            <List header={'我的订单' + ShopList.length}>
                {ShopList.map((item: any) => (
                    <List.Item key={item.id}>
                        {item.name}-------------<b onClick={() => del(item.id)}>删除订单</b>
                    </List.Item>
                ))}
            </List>
        </div>
    )
}
