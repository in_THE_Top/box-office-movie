import React, { useState, useRef } from "react";
import { SearchBar, Grid, Toast, List } from "antd-mobile";
import { SearchBarRef } from "antd-mobile/es/components/search-bar";
import { LeftOutline } from "antd-mobile-icons";
import { SCinema, SearchCinema } from "./api";
import { Link } from "react-router-dom";
import "./search.scss";
export default function Index() {
  const click1 = () => {
    window.history.go(-1);
  };
  //搜索功能
  const searchRef = useRef<SearchBarRef>(null);
  const [Yingyuan, setYingyuan] = useState([]);
  const [value, setValue] = useState<any>("");
  const [look, setLook] = useState([]);
  //获取搜索影院
  const search = async (value: any) => {
    let res = await SearchCinema(value);
    setLook(res.data);
    console.log(look);
  };
  return (
    <div>
      <div className="Inp">
        <span onClick={click1} className="icon">
          {" "}
          <LeftOutline />{" "}
        </span>
        <SearchBar
          ref={searchRef}
          placeholder="请输入内容"
          showCancelButton={() => true}
          onSearch={(value) => {
            search(value);
          }}
          onChange={(value) => {
            setValue(value);
          }}
          style={{ height: 50, width: "80%" }}
        />
      </div>
      <List header="您的搜索结果">
        {look.map((item: any, index: Number) => (
          <List.Item key={item.id}>
            <a
              href="http://localhost:3001/cinemas/detail/8119"
              style={{ fontSize: "15px", color: "black" }}
            >
              {item.name}
            </a>
          </List.Item>
        ))}
      </List>
      <div className="border">
        <p className="P">离你最近</p>
        <ul>
          <Grid columns={3} gap={8}>
            <Grid.Item>
              <div className="grid-demo-item-block">
                英皇电影城（东海缤纷店）
              </div>
            </Grid.Item>
            <Grid.Item>
              <div className="grid-demo-item-block">江湖影院侨香店</div>
            </Grid.Item>
            <Grid.Item>
              <div className="grid-demo-item-block">风云国际影城</div>
            </Grid.Item>
            <Grid.Item>
              <div className="grid-demo-item-block">
                深圳星美国际影商城福田体育公园店
              </div>
            </Grid.Item>
            <Grid.Item>
              <div className="grid-demo-item-block">
                中影国际影城（深圳印力中心店）
              </div>
            </Grid.Item>
          </Grid>
        </ul>
      </div>
    </div>
  );
}
