import React, { useEffect, useState } from "react";
import axios from "axios";
import { URL, inter } from '../../api'
import { useNavigate } from "react-router-dom";
import style from "./index.module.css";

const Login = () => {
  const navigate = useNavigate()
  const [val, setVal] = useState({
    name: '',
    password: ''
  })
  const add = (val) => {
    axios.get(URL + inter + 'user?', {
      params: {
        name: val.name,
        password: val.password
      }
    }).then(res => {
      if (val.name === res.data[0].name && val.password === res.data[0].password) {
        sessionStorage.setItem('name', res.data[0].name)
        setTimeout(() => {
          navigate('/films')
        }, 2000)
        alert('登录成功')
      } else {
        alert('登录失败')
      }
    })
  }
  return (
    <div>
      <ul className={style.ul}>
        <img
          className={style.img}
          src="	https://assets.maizuo.com/h5/mz-auth/public/app/img/logo.19ca0be.png"
          alt=""
        />
        <li>
          <input type="text" placeholder="用户名" className={style.input} defaultValue={val.name} onChange={(e) => {
            val.name = e.target.value
          }} />
        </li>
        <li>
          <input type="password" placeholder="密码" className={style.input} defaultValue={val.password} onChange={(e) => {
            val.password = e.target.value
          }} />
        </li>
        <button className={style.button} onClick={() => { add(val) }}>登录</button>
      </ul>
    </div >
  );
};
export default Login;

