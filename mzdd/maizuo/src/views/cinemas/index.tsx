// import axios from "axios";
import React, { useState, useEffect, useRef, } from "react";
import { getCinemaList, SCinema, SearchCinema } from "./api";
import style from "./home.module.scss";
import { NavLink } from "react-router-dom";
import { SearchBar, Popup, Button, Space, Toast, List } from 'antd-mobile'
import { AntOutline, SearchOutline } from "antd-mobile-icons";
import { SearchBarRef } from 'antd-mobile/es/components/search-bar'
import Amap from "./Amap/index";
import menu from "../../hoc/menu";
import Swiper1 from "./Swiper";
const { header, ul, address } = style;
function Films() {
  //搜索功能
  const searchRef = useRef<SearchBarRef>(null)
  const [Yingyuan, setYingyuan] = useState([])
  const [value, setValue] = useState<any>('')
  const [look, setLook] = useState([])
  // const [searchData, setSearchData] = useState([])
  const [cinema, setCinema] = useState([]);
  const [visible2, setVisible2] = useState(false)
  //获取搜索影院
  const search = async (value: any) => {
    let res = await SearchCinema(value)
    setLook(res.data)
  }
  //影院列表
  const getCinema = async () => {
    let res = await getCinemaList();
    console.log(res,'影院数据')
    setCinema(res.data);
  };
  useEffect(() => {
    getCinema();
  }, []);
  return (
    <div>
      <div className={header}>
        <span>北京</span>
        <span>影院</span>
        <span>

          {/* <SearchBar
          placeholder='请输入内容'
          style={{ '--background': '#ffffff' }}
        /> */}
          <Button
            onClick={() => {
              setVisible2(true)
            }}
            style={{ backgroundColor: 'rgb(193, 191, 191)', border: 0 }}>
            <SearchOutline fontSize={24} />
          </Button>
          <Popup
            visible={visible2}
            onMaskClick={() => {
              setVisible2(false)
            }}
            position='top'
            bodyStyle={{ minHeight: '20vh'}}
          >
            <Space block direction='vertical'>
              <SearchBar
                ref={searchRef}
                placeholder='请输入内容'
                showCancelButton
                onSearch={value => {
                  Toast.show(`你搜索了：${value}`)
                  search(value)
                }}
                // onClear={() => {
                //   Toast.show('清空内容')
                // }}
                // onCancel={() => {
                //   Toast.show('取消搜索')
                // }}
                onChange={value => {
                  setValue(value)
                }}
                style={{height:50}}
              />
            </Space>
            <List header='您的搜索结果'>
             {
              look.map((item: any, index: Number) => (
                  <List.Item key={item.id} >
                    <NavLink to={{ pathname: "detail/" + item.id }} style={{ fontSize: '15px', color: "black" }}>{item.name}</NavLink>
                  </List.Item>
                )
                )
              }
            </List>
          </Popup>
        </span>
      </div>
      <div style={{ height: 50 }}></div>
      <map />
      <Amap />
      <ul className={ul}>
        {cinema.map((item: any) => (
          <li key={item.id}>
            <div>
              <p><NavLink to={{ pathname: "detail/" + item.id }} style={{ fontSize: '15px', color: "black" }}>{item.name}</NavLink></p>
              <span >{item.address}</span>
            </div>
            <div>
              <b>
                ￥<em>{item.lowPrice / 100}</em>起
              </b>
              <span>
                距离：<em>未知</em>
              </span>
            </div>
          </li>
        ))}
      </ul>
    </div >
  );
}

export default menu(Films);
