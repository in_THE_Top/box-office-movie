import React, { useState, useEffect } from "react";

import Swiper from "swiper";
import {LocationOutline} from 'antd-mobile-icons'
import "swiper/css/swiper.css";
import { getBannerList } from "../api";
import style from "./home.scss";
// const { banner } = style;
export default function SwiperComponet() {
  const [bannerList, setBannerList] = useState([]);

  const getBanner = () => {
    getBannerList().then((res) => {
      console.log(res, "bannerList1");
      setBannerList(res.data);
    });
  };
  useEffect(() => {
    getBanner();
  }, []);

  useEffect(() => {
    //初始化Swiper
    new Swiper('.swiper-container', {
      slidesPerView: 4,
      spaceBetween: 30,
      centeredSlides: true,
      // loop: true,
      // pagination: {
      //   el: '.swiper-pagination',
      //   clickable: true,
      // },
    });
  }, [bannerList]);
  return (
    <div className='banner'>
      {bannerList.length > 0 && (
        <div className="swiper-container">
          <div className="swiper-wrapper">
            {bannerList.map((item, i) => (
              <div key={i} className="swiper-slide">
                <img src={item.poster} alt="" />
               
              </div>
              
            ))}
          </div>
          <div className="swiper-pagination">
          </div>
        <div className="jiantou"><LocationOutline/></div>
        </div>

      )}
    </div>
  );
}
