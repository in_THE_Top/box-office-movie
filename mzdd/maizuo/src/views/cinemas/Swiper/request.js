import axios from "axios";
import {URL } from "../../../api"
const url = URL;
const request = axios.create({
  baseURL: url,
  timeout: 5000,
  //   withCredentials: true, // 没有使用这个 这个可以自动携带cookie, 看浏览器支不支持cookie
});

// 请求拦截器
request.interceptors.request.use(
  (config) => {
    // post请求需要添加时间戳
    config.url = config.url + "?timestamp=" + Date.now();
    //  获取cookie
    let cookie = localStorage.getItem("cookie") || "";
    // 如果不带cookie 出现301   访问无权限
    if (config.method.toLocaleLowerCase() == "get") {
      config.params = { ...config.params, cookie };
    } else {
      config.data = { ...config.data, cookie };
    }
    //没有做逻辑
    return config;
  },
  (error) => {
    //对请求错误做些什么
    return Promise.reject(error);
  }
);

export default request;
