import React, { useState, useEffect } from "react";
import { InHot } from "./type";
import "./home.scss";
import axios from "axios";
import { getBannerList } from "../api";
import { useParams, NavLink } from "react-router-dom";
import { URL, inter } from "../../../api";
import {
  LeftOutline,
  RightOutline,
  EnvironmentOutline,
  PhoneFill,
} from "antd-mobile-icons";
import {
  Collapse,
  DotLoading,
  Result,
  JumboTabs,
  Empty,
  Popup,
  Button,
} from "antd-mobile";
import Swiper1 from "../Swiper";
export default function Index() {
  const DynamicContent = () => {
    const [finished, setFinished] = useState(false);
    return finished ? (
      <Result status="success" title="处理成功" />
    ) : (
      <DotLoading />
    )
  }
  //添加购买电影
  const addShop = (item: any) => {
    console.log(item)
    axios.post(URL + inter + "filmsShop", {
      name: item.name,
      id: item.id,
      count: 1
    });
  };


  //设置时间
  function today() {
    var day = new Date();
    day.setTime(day.getTime());
    var s2 = day.getMonth() + 1 + "月" + day.getDate() + "日";
    return s2;
  }
  //明天的时间
  function tomorrow() {
    var day = new Date();

    day.setTime(day.getTime() + 24 * 60 * 60 * 1000);
    var s3 = day.getMonth() + 1 + "月" + day.getDate() + "日";
    return s3;
  }
  //后天的时间
  function afterTomorrow() {
    var day = new Date();
    day.setTime(day.getTime() + 24 * 60 * 60 * 1000);
    var s3 = day.getMonth() + 1 + "月" + (day.getDate() + 1) + "日";
    return s3;
  }
  //------------
  const [visible2, setVisible2] = useState(false);
  //------------
  const [filmsList, setFilmsList] = useState([]);
  const getFilms = () => {
    getBannerList().then((res) => {
      setFilmsList(res.data);
    });
  };
  let { id } = useParams();
  // console.log(id,'id')
  const [details, setDetails] = useState<InHot>({});
  const getHot = async () => {
    let res = await axios.get(URL + inter + "cinemas/" + id);
    setDetails(res.data);
  };
  useEffect(() => {
    getHot();
    getFilms();
  }, []);
  const Back = () => {
    window.history.go(-1);
  };
  return (
    <div>
      <div className="header">
        <div className="back">
          <NavLink to={{ pathname: "/cinemas" }}>
            <LeftOutline
              fontSize={100}
              color="var(--adm-color-danger)"
              onClick={Back}
            />
          </NavLink>
        </div>
        <div className="cinimaName">
          <div>
            <p>{details.name}</p>
            <p>
              <span>停车位</span>
              <span>儿童票</span>
              <span>3D电影</span>
              <Button
                onClick={() => {
                  setVisible2(true);
                }}
                style={{ fontSize: "10px", border: 0 }}
              >
                <RightOutline
                  color="rgb(243, 148, 40)"
                  style={{ border: 0, fontSize: "12px" }}
                />
              </Button>
              <Popup
                visible={visible2}
                onMaskClick={() => {
                  setVisible2(false);
                }}
                position="top"
                bodyStyle={{ minHeight: "30vh", padding: "30px" }}
              >
                <p>
                  <span
                    style={{
                      color: " #ffb232",
                      display: "inline-block",
                      marginRight: "15px",
                      border: "1px solid #ffb232",
                    }}
                  >
                    停车位
                  </span>
                  院内免费停车
                </p>
                <p style={{ marginTop: "10px", marginBottom: "10px" }}>
                  <span
                    style={{
                      color: " #ffb232",
                      display: "inline-block",
                      border: "1px solid #ffb232",
                      marginRight: "15px",
                    }}
                  >
                    儿童票
                  </span>
                  1.3以下儿童观看2D电影免票无座
                </p>
                <p>
                  <span
                    style={{
                      color: " #ffb232",
                      display: "inline-block",
                      marginRight: "15px",
                      border: "1px solid #ffb232",
                    }}
                  >
                    3D电影
                  </span>
                  免押金
                </p>
              </Popup>
            </p>
          </div>
          <div className="adress">
            <span>
              <EnvironmentOutline />
            </span>
            <span className="address1"> {details.address}</span>
            <span>
              <PhoneFill />
            </span>
          </div>
        </div>
      </div>
      <Swiper1 />
      <Collapse defaultActiveKey={['1']}>
        {
          filmsList.map((item: any) => (
            <Collapse.Panel key={item.id} title={+item.grade ? '《' + item.name + '》' + item.grade : '《' + item.name + '》' + '   暂无评分'} style={{ fontSize: '14px' }}>
              <JumboTabs>
                <JumboTabs.Tab title='今天' description={today()} key='fruits'>
                  <ul>
                    <li id='list' style={{
                      height: '20px',
                      borderBottom: ' 1px solid #ccc',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      padding: '15px 0'
                    }}>
                      <div>
                        <p style={{
                          marginBottom: '-1px',
                          marginTop: '-5px',
                        }}><span style={{ color: 'black', fontSize: '15px' }}>10:15</span></p>
                        <span><span>12:04</span>散场</span>
                      </div>
                      <div>
                        <p style={{
                          marginBottom: '-1px',
                          marginTop: '-5px'
                        }}><span style={{ color: 'black', fontSize: '15px' }}>国语2D</span></p>
                        <span>2号厅</span>
                      </div>
                      <div style={{
                        width: '40%',
                        textAlign: 'right',
                        lineHeight: '25px',
                        color: '#ff5f16'
                      }}>
                        <span>￥<b>36.9</b></span>
                        <em style={{
                          display: 'inline-block',
                          width: '50px',
                          fontStyle: ' normal',
                          border: ' 1px solid #ff5f16',
                          textAlign: 'center',
                          marginLeft: '30px'
                        }} onClick={() => { addShop(item) }}>购票</em>
                      </div>
                    </li>
                    <li id='list' style={{
                      height: '50px',

                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      padding: '15px 0'
                    }}>
                      <div>
                        <p style={{
                          marginBottom: '-1px',
                          marginTop: '-5px'
                        }}><span style={{ color: 'black', fontSize: '15px' }}>19:30</span></p>
                        <span><span>21:19</span>散场</span>
                      </div>
                      <div>
                        <p style={{
                          marginBottom: '-1px',
                          marginTop: '-5px'
                        }}><span style={{ color: 'black', fontSize: '15px' }}>国语2D</span></p>
                        <span>8号厅</span>
                      </div>
                      <div style={{
                        width: '40%',
                        textAlign: 'right',
                        lineHeight: '25px',
                        color: '#ff5f16'
                      }}>
                        <span>￥<b>38.9</b></span>
                        <em style={{
                          display: 'inline-block',
                          width: '50px',
                          fontStyle: ' normal',
                          border: ' 1px solid #ff5f16',
                          textAlign: 'center',
                          marginLeft: '30px'
                        }} onClick={() => { addShop(item) }}>购票</em>
                      </div>
                    </li>
                  </ul>
                </JumboTabs.Tab>
                <JumboTabs.Tab title='明天' description={tomorrow()} key='vegetables'>
                  <Empty description='暂无场次' />
                </JumboTabs.Tab>
                <JumboTabs.Tab title='后天' description={afterTomorrow()} key='animals'>
                  <Empty description='暂无场次' />
                </JumboTabs.Tab>
              </JumboTabs>
            </Collapse.Panel>
          ))
        }
      </Collapse>
      <Collapse defaultActiveKey={["1"]}>
        {filmsList.map((item: any) => (
          <Collapse.Panel
            key={item.id}
            title={
              +item.grade
                ? "《" + item.name + "》" + item.grade
                : "《" + item.name + "》" + "   暂无评分"
            }
            style={{ fontSize: "14px" }}
          >
            <JumboTabs>
              <JumboTabs.Tab title="今天" description={today()} key="fruits">
                <ul>
                  <li
                    id="list"
                    style={{
                      height: "20px",
                      borderBottom: " 1px solid #ccc",
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "space-between",
                      padding: "15px 0",
                    }}
                  >
                    <div>
                      <p
                        style={{
                          marginBottom: "-1px",
                          marginTop: "-5px",
                        }}
                      >
                        <span style={{ color: "black", fontSize: "15px" }}>
                          10:15
                        </span>
                      </p>
                      <span>
                        <span>12:04</span>散场
                      </span>
                    </div>
                    <div>
                      <p
                        style={{
                          marginBottom: "-1px",
                          marginTop: "-5px",
                        }}
                      >
                        <span style={{ color: "black", fontSize: "15px" }}>
                          国语2D
                        </span>
                      </p>
                      <span>2号厅</span>
                    </div>
                    <div
                      style={{
                        width: "40%",
                        textAlign: "right",
                        lineHeight: "25px",
                        color: "#ff5f16",
                      }}
                    >
                      <span>
                        ￥<b>36.9</b>
                      </span>
                      <em
                        style={{
                          display: "inline-block",
                          width: "50px",
                          fontStyle: " normal",
                          border: " 1px solid #ff5f16",
                          textAlign: "center",
                          marginLeft: "30px",
                        }}
                      >
                        购票
                      </em>
                    </div>
                  </li>
                  <li
                    id="list"
                    style={{
                      height: "50px",

                      display: "flex",
                      alignItems: "center",
                      justifyContent: "space-between",
                      padding: "15px 0",
                    }}
                  >
                    <div>
                      <p
                        style={{
                          marginBottom: "-1px",
                          marginTop: "-5px",
                        }}
                      >
                        <span style={{ color: "black", fontSize: "15px" }}>
                          19:30
                        </span>
                      </p>
                      <span>
                        <span>21:19</span>散场
                      </span>
                    </div>
                    <div>
                      <p
                        style={{
                          marginBottom: "-1px",
                          marginTop: "-5px",
                        }}
                      >
                        <span style={{ color: "black", fontSize: "15px" }}>
                          国语2D
                        </span>
                      </p>
                      <span>8号厅</span>
                    </div>
                    <div
                      style={{
                        width: "40%",
                        textAlign: "right",
                        lineHeight: "25px",
                        color: "#ff5f16",
                      }}
                    >
                      <span>
                        ￥<b>38.9</b>
                      </span>
                      <em
                        style={{
                          display: "inline-block",
                          width: "50px",
                          fontStyle: " normal",
                          border: " 1px solid #ff5f16",
                          textAlign: "center",
                          marginLeft: "30px",
                        }}
                      >
                        购票
                      </em>
                    </div>
                  </li>
                </ul>
              </JumboTabs.Tab>
              <JumboTabs.Tab
                title="明天"
                description={tomorrow()}
                key="vegetables"
              >
                <Empty description="暂无场次" />
              </JumboTabs.Tab>
              <JumboTabs.Tab
                title="后天"
                description={afterTomorrow()}
                key="animals"
              >
                <Empty description="暂无场次" />
              </JumboTabs.Tab>
            </JumboTabs>
          </Collapse.Panel>
        ))}
      </Collapse>
    </div>
  );
}
