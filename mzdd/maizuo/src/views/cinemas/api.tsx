import axios from "axios";
import { URL, inter } from "../../api";
/**
 * json-server所有的请求都是get
 *  查看全部都是get
 *
 */
//获取影院名单
// export const getCinemaList = () => axios.get("http://localhost:3000/cinema");
export const getCinemaList = () => axios.get(URL + inter + "cinemas");

//获取影院电影信息
export const getBannerList = () => axios.get(URL + inter + "films");

//获取搜索影院

export const SCinema = () => axios.get(URL + inter + "cinemas", {})

//查询
export const SearchCinema = (name: any) =>axios.get(URL + inter + "cinemas"+`?q=${name} `)