import React from "react";
import { useLocation, Outlet, NavLink } from "react-router-dom";
import style from "./films.module.css";
import menu from "../../hoc/menu";
// const {nav}=style
const Films = ({ menu }: any) => {
  console.log(menu, "menu");
  const location = useLocation();
  console.log(location, "lcation");
  return (
    <div>
      <ul className={style.nav}>
        {menu.map((item: any) => (
          <li key={item.path}>
            <NavLink to={"/films/" + item.path}>{item.til}</NavLink>
          </li>
        ))}
      </ul>
      <div style={{ height: "50px", background: "#fff" }}></div>
      <Outlet></Outlet>
    </div>
  );
};

export default menu(Films);
