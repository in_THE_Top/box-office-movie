import React, { useState, useEffect } from "react";
import axios from "axios";
import style from "./nowPlaying.module.css";
// import { URL, inter } from "../../../api";
import { NavLink } from "react-router-dom";
import { InHot } from "./type";
export default function NowPlaying() {
  const [Hot, setHot] = useState<InHot[]>([]);
  const getHot = () => {
    axios.get(" http://localhost:3000/films").then((res) => {
      console.log(res, "123");
      setHot(res.data);
    });
  };
  useEffect(() => {
    getHot();
  }, []);

  return (
    <div>
      <ul>
        {Hot.map((item, i) => (
          <li className={style.list} key={item.id}>
            <div>
              <img src={item.poster} alt="" />
            </div>
            <div style={{ flex: "1" }}>
              <NavLink to={"/details/" + item.id}>
                <div className={style.center}>
                  <dl>
                    <dt>
                      <b>{item.name}</b>
                      {item.filmType.name}
                    </dt>
                    <dd>观众评分：{item.grade}</dd>
                    <dd className={style.star}>
                      主演：
                      {item.actors.map((ite, i) => (
                        <span key={i}>{ite.name}</span>
                      ))}
                    </dd>
                  </dl>
                  <dl>
                    {item.nation} | {item.runtime}
                  </dl>
                </div>
              </NavLink>
            </div>
            <div>
              <NavLink to={"/playing/" + item.id}>
                <span className={style.shop}>购票</span>
              </NavLink>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
}
