import React, { useEffect, useState } from 'react'
import {getSoonFilms} from "./api"
import {filmsing_inter} from "./type"
import Link from './link'
import style from "./css/comingSoon.module.css"
const {comingList} = style
export default function ComingSoon() {
  const [films,setFilms] = useState<filmsing_inter[]>([])
  const getList = async ()=>{
    let res =await getSoonFilms()
    console.log(res.data,"getSoonFilms");
    setFilms(res.data)
  }
  useEffect(()=>{
      getList()
  },[])
  return (

    // // import {useNavigate} from "react-router-dom"
                
            // 使用起来起来就是  const navigate= useNavigate()
            // navigator("地址")
    <div className={comingList}>
      {
        films.map((ele,i)=>{
          return <div  key={i}>
           <Link film={ele}  />
          <hr />
          </div>
        })
      }
    </div>
  )
}
