import axios from 'axios'
import React, { useEffect, useRef, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { getSoonFilms } from "../api"
import { URL, inter } from "../../../../api"
import { LeftOutline } from "antd-mobile-icons"
import moment from "moment";
import style from "../css/SoonDetails.module.css"
const { top, topImg, returns, main, returns_cen,btn } = style
interface obj {
  [propName: string]: any;
}
export default function SoonDetails() {
  const [show, switchShow] = useState(false);
  // 使用ref
  // const TopRef= useRef(null)
  // // 编程式路由
  const navigate = useNavigate()
  // 接受父组件传输的id
  const params = useParams()

  const goSoonShop=()=>{
    navigate(`/soonshop/`+params.id)
  }


  const [actor,setActor] = useState<obj[]>([])
  const [val, setVal] = useState<obj>({})
  // 根据父组件传输的id请求书
  const get = async () => {
    let res = await axios.get(URL + inter + "filmsing/" + params.id)
    console.log(res, "SoonDetails_res");
    setVal(res.data)
    setActor(res.data.actors)
  }
  // 返回路由
  const goSoon = (e) => {
    navigate("/films/comingSoon")
  }



  useEffect(() => {
    get()
    const listener = () => {
      switchShow(window.scrollY > 50)
    };
    document.addEventListener('scroll', listener);
    return () => document.removeEventListener('scroll', listener);
  }, [])


  useEffect(() => {
    console.log(val.actors)

  }, [val])

  return (
    <div  >

      <div className={returns} style={{ display: show ? "block" : "none" }}   >
        <LeftOutline onClick={(e) => {
          goSoon(e)
        }} />
        <p className={returns_cen}>
          {val.name}
        </p>
      </div>
      <div className={top}>

        <img src={val.poster} alt="" className={topImg} />
      </div>

      <div className={main}>
        <div className={style.details_body}>
          {/* 名字，评分 */}
          <p>
            <span style={{ fontSize: 20, color: "#191a1b" }}>
              {" "}
              {val.name}
            </span>
            <span style={{ float: "right", color: "#ffb232" }}>
              预测评分：8分
            </span>
          </p>
          {/* 类型 */}
          <p style={{ fontSize: "14px", color: "#797d82" }}>{val.category}</p>
          {/* 上映时间 */}
          <p style={{ fontSize: "13px", color: "#797d82" }}>
            {moment(val.premiereAt * 1000).format("YYYY")}年上映
          </p>
          {/* 时长 */}
          <p style={{ fontSize: "13px", color: "#797d82" }}>
            {val.nation} | {val.runtime}
          </p>
          {/* 简介 */}
          <p style={{ fontSize: "13px", color: "#797d82" }}>{val.synopsis}</p>

        </div>
      </div>
        
        <div className={style.actors}>
            {actor.map((ele,i)=>{
              return <div key={i}>
                    <img src={ele.avatarAddress} alt="" />
                    <p>
                      {ele.name}
                    </p>
                    <p>
                      {ele.role}
                    </p>
              </div>
            })}

      
        </div>

        <div onClick={()=>{
              goSoonShop()
        }} className={btn}>点击购票</div>
    </div>
  )
}

