import React from 'react'
import style from "./css/comingSoon.module.css"
import {useNavigate} from "react-router-dom"
import { filmsing_inter } from "./type"
const { imges, soonLink ,LinkBox,filmShop,name,filmType,nation,actor} = style
interface interLink{
  film:filmsing_inter
}
export default function Link({ film }:interLink ) {
  const navigate= useNavigate()
  const go_details=(e,id)=>{
            console.log(e.target.nodeName,"e.target");
            if(e.target.nodeName=="BUTTON"){
              navigate(`/soonshop/`+id)
            }else{
              navigate(`/soondetails/`+id)
            }
  }
  return (
    <div className={soonLink} onClick={(e)=>{
      console.log(1212);
      
           go_details(e,film.id)
    }}>
      <img className={imges} src={film.poster} alt="" />
      <div className={LinkBox}>
      <p><span className={name}>{film.name}</span> <span className={filmType}>{film.filmType.name}</span> </p>
      <p className={actor}>主演:{film.actors.map((ele,i)=>{
          return   <span key={i}>&nbsp;{ele.name}&nbsp;</span>
      })}</p>
      <p className={nation}>
        {film.nation}|{film.id}
      </p>

      <button className={filmShop}>
        购票
      </button>
      </div>
    </div>
  )
}
