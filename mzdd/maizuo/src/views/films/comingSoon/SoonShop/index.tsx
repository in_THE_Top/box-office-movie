import React, { useState, useEffect, useRef } from "react";
import style from "./details.module.css";
import { NavLink } from "react-router-dom";
import {
  LeftOutline,
  SearchOutline,
  ArrowDownCircleOutline,
  DownOutline,
  SmileOutline,
} from "antd-mobile-icons";
import axios from "axios";
import { InHot } from "./type";
import { useNavigate, useParams } from "react-router-dom";
import { SearchBar, Grid } from "antd-mobile";
import {
  JumboTabs,
  Dropdown,
  Radio,
  Space,
  Button,
  CheckList,
} from "antd-mobile";
import { DropdownRef } from "antd-mobile/es/components/dropdown";
export default function Index() {
  const [aa, setaa] = useState("最近去过");

  const ref = useRef<DropdownRef>(null);
  let { id } = useParams();
  const [details, setDetails] = useState<InHot>({});
  // 影院渲染
  const [cinem, setCinem] = useState<InHot[]>([]);
  const navgiate = useNavigate();

  const getHot = async () => {
    let res = await axios.get("http://localhost:3000/filmsing/" + id);
    console.log(res.data.filmType.name, "filmType");
    setDetails(res.data);
  };
  // 影院渲染
  const getCinem = async () => {
    let res = await axios.get("http://localhost:3000/cinemas");
    console.log(res.data, "cnims");
    setCinem(res.data);
  };

  useEffect(() => {
    getHot();
    getCinem();
    // console.log(details);
  }, []);

  const click = () => {
    navgiate("/films");
  };
  const click1 = () => {
    navgiate("/search");
  };
  return (
    <div>
      <div className={style.header}>{details.name}</div>
      <b className={style.iconleft}>
        <LeftOutline fontSize={20} onClick={click} />
      </b>
      <b className={style.iconright}>
        <SearchOutline fontSize={20} onClick={click1} />
      </b>
      {/* 选项卡 */}
      <div style={{ marginTop: "50px" }}>
        <Dropdown ref={ref}>
          <Dropdown.Item key="sorter" title="全城">
            <div style={{ padding: 12 }}>
              <Grid columns={3} gap={8}>
                <Grid.Item>
                  <div className="grid-demo-item-block">大东区</div>
                </Grid.Item>
                <Grid.Item>
                  <div className="grid-demo-item-block">铁西区</div>
                </Grid.Item>
                <Grid.Item>
                  <div className="grid-demo-item-block">于洪区</div>
                </Grid.Item>
                <Grid.Item>
                  <div className="grid-demo-item-block">和平区</div>
                </Grid.Item>
                <Grid.Item>
                  <div className="grid-demo-item-block">浑南区</div>
                </Grid.Item>
                <Grid.Item>
                  <div className="grid-demo-item-block">苏家屯</div>
                </Grid.Item>
                <Grid.Item>
                  <div className="grid-demo-item-block">沈河区</div>
                </Grid.Item>
              </Grid>
            </div>
          </Dropdown.Item>
          <Dropdown.Item key="bizop" title={aa}>
            <div style={{ padding: 12 }}>
              <CheckList defaultValue={["aa"]}>
                <CheckList.Item value="aa">
                  <p
                    color="primary"
                    onClick={() => {
                      ref.current?.close();
                      setaa("最近去过");
                    }}
                  >
                    最近去过
                  </p>
                </CheckList.Item>

                <CheckList.Item value="离我最近">
                  <p
                    color="primary"
                    onClick={() => {
                      ref.current?.close();
                      setaa("离我最近");
                    }}
                  >
                    离我最近
                  </p>
                </CheckList.Item>
              </CheckList>
            </div>
          </Dropdown.Item>
        </Dropdown>

        <JumboTabs defaultActiveKey="1">
          <p>123</p>
          <JumboTabs.Tab title="今天9月19日" description="" key="1">
            <ul>
              {cinem.map((item, i) => (
                <NavLink to={{ pathname: "/cinemas/detail/" + item.id }}>
                  <li
                    key={item.id}
                    style={{
                      padding: "10px",
                      boxSizing: "border-box",
                      display: "flex",
                      justifyContent: " space-between",
                    }}
                  >
                    <div style={{ width: "calc(100vw - 100px)" }}>
                      <p
                        style={{
                          fontSize: "16px",
                          color: "#191a1b",
                          overflow: "hidden",
                          marginBottom: "5px",
                          textOverflow: "ellipsis",
                          whiteSpace: "nowrap",
                        }}
                      >
                        {item.name}
                      </p>
                      <p
                        style={{
                          fontSize: "13px",
                          color: "#797d82",
                          overflow: "hidden",
                          textOverflow: "ellipsis",
                          whiteSpace: "nowrap",
                        }}
                      >
                        {item.adress}
                      </p>
                    </div>
                    <div
                      style={{
                        color: "#ff5f16",
                        width: "70px",
                        textAlign: "right",
                      }}
                    >
                      <p style={{ marginBottom: "5px" }}>
                        ￥{item.lowPrice / 1000}起
                      </p>
                      <p
                        style={{
                          color: "#797d82",
                          fontSize: "10px",
                          lineHeight: "26px",
                        }}
                      >
                        距离{item.distance}
                      </p>
                    </div>
                  </li>
                </NavLink>
              ))}
            </ul>
          </JumboTabs.Tab>
          <JumboTabs.Tab title="明天9月20日" description="" key="2">
            <ul>
              {cinem.map((item, i) => (
                <li
                  key={i}
                  style={{
                    padding: "10px",
                    boxSizing: "border-box",
                    display: "flex",
                    justifyContent: " space-between",
                  }}
                >
                  <div style={{ width: "calc(100vw - 100px)" }}>
                    <p
                      style={{
                        fontSize: "16px",
                        color: "#191a1b",
                        overflow: "hidden",
                        marginBottom: "5px",
                        textOverflow: "ellipsis",
                        whiteSpace: "nowrap",
                      }}
                    >
                      {item.name}
                    </p>
                    <p
                      style={{
                        fontSize: "13px",
                        color: "#797d82",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                        whiteSpace: "nowrap",
                      }}
                    >
                      {item.adress}
                    </p>
                  </div>
                  <div
                    style={{
                      color: "#ff5f16",
                      width: "70px",
                      textAlign: "right",
                    }}
                  >
                    <p style={{ marginBottom: "5px" }}>
                      ￥{item.lowPrice / 1000}起
                    </p>
                    <p
                      style={{
                        color: "#797d82",
                        fontSize: "10px",
                        lineHeight: "26px",
                      }}
                    >
                      距离{item.distance}
                    </p>
                  </div>
                </li>
              ))}
            </ul>
          </JumboTabs.Tab>
          <JumboTabs.Tab title="后天9月21日" description="" key="3">
            <ul>
              {cinem.map((item, i) => (
                <li
                  key={item.id}
                  style={{
                    padding: "10px",
                    boxSizing: "border-box",
                    display: "flex",
                    justifyContent: " space-between",
                  }}
                >
                  <div style={{ width: "calc(100vw - 100px)" }}>
                    <p
                      style={{
                        fontSize: "16px",
                        color: "#191a1b",
                        overflow: "hidden",
                        marginBottom: "5px",
                        textOverflow: "ellipsis",
                        whiteSpace: "nowrap",
                      }}
                    >
                      {item.name}
                    </p>
                    <p
                      style={{
                        fontSize: "13px",
                        color: "#797d82",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                        whiteSpace: "nowrap",
                      }}
                    >
                      {item.adress}
                    </p>
                  </div>
                  <div
                    style={{
                      color: "#ff5f16",
                      width: "70px",
                      textAlign: "right",
                    }}
                  >
                    <p style={{ marginBottom: "5px" }}>
                      ￥{item.lowPrice / 1000}起
                    </p>
                    <p
                      style={{
                        color: "#797d82",
                        fontSize: "10px",
                        lineHeight: "26px",
                      }}
                    >
                      距离{item.distance}
                    </p>
                  </div>
                </li>
              ))}
            </ul>
          </JumboTabs.Tab>
          <JumboTabs.Tab title="周四9月22日" description="" key="4">
            <ul>
              {cinem.map((item, i) => (
                <li
                  key={item.id}
                  style={{
                    padding: "10px",
                    boxSizing: "border-box",
                    display: "flex",
                    justifyContent: " space-between",
                  }}
                >
                  <div style={{ width: "calc(100vw - 100px)" }}>
                    <p
                      style={{
                        fontSize: "16px",
                        color: "#191a1b",
                        overflow: "hidden",
                        marginBottom: "5px",
                        textOverflow: "ellipsis",
                        whiteSpace: "nowrap",
                      }}
                    >
                      {item.name}
                    </p>
                    <p
                      style={{
                        fontSize: "13px",
                        color: "#797d82",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                        whiteSpace: "nowrap",
                      }}
                    >
                      {item.adress}
                    </p>
                  </div>
                  <div
                    style={{
                      color: "#ff5f16",
                      width: "70px",
                      textAlign: "right",
                    }}
                  >
                    <p style={{ marginBottom: "5px" }}>
                      ￥{item.lowPrice / 1000}起
                    </p>
                    <p
                      style={{
                        color: "#797d82",
                        fontSize: "10px",
                        lineHeight: "26px",
                      }}
                    >
                      距离{item.distance}
                    </p>
                  </div>
                </li>
              ))}
            </ul>
          </JumboTabs.Tab>
          <JumboTabs.Tab title="周五9月23日" description="" key="5">
            <ul>
              {cinem.map((item, i) => (
                <li
                  key={item.id}
                  style={{
                    padding: "10px",
                    boxSizing: "border-box",
                    display: "flex",
                    justifyContent: " space-between",
                  }}
                >
                  <div style={{ width: "calc(100vw - 100px)" }}>
                    <p
                      style={{
                        fontSize: "16px",
                        color: "#191a1b",
                        overflow: "hidden",
                        marginBottom: "5px",
                        textOverflow: "ellipsis",
                        whiteSpace: "nowrap",
                      }}
                    >
                      {item.name}
                    </p>
                    <p
                      style={{
                        fontSize: "13px",
                        color: "#797d82",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                        whiteSpace: "nowrap",
                      }}
                    >
                      {item.adress}
                    </p>
                  </div>
                  <div
                    style={{
                      color: "#ff5f16",
                      width: "70px",
                      textAlign: "right",
                    }}
                  >
                    <p style={{ marginBottom: "5px" }}>
                      ￥{item.lowPrice / 1000}起
                    </p>
                    <p
                      style={{
                        color: "#797d82",
                        fontSize: "10px",
                        lineHeight: "26px",
                      }}
                    >
                      距离{item.distance}
                    </p>
                  </div>
                </li>
              ))}
            </ul>
          </JumboTabs.Tab>
          <JumboTabs.Tab title="周六9月24日" description="" key="6">
            <ul>
              {cinem.map((item, i) => (
                <li
                  key={item.id}
                  style={{
                    padding: "10px",
                    boxSizing: "border-box",
                    display: "flex",
                    justifyContent: " space-between",
                  }}
                >
                  <div style={{ width: "calc(100vw - 100px)" }}>
                    <p
                      style={{
                        fontSize: "16px",
                        color: "#191a1b",
                        overflow: "hidden",
                        marginBottom: "5px",
                        textOverflow: "ellipsis",
                        whiteSpace: "nowrap",
                      }}
                    >
                      {item.name}
                    </p>
                    <p
                      style={{
                        fontSize: "13px",
                        color: "#797d82",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                        whiteSpace: "nowrap",
                      }}
                    >
                      {item.adress}
                    </p>
                  </div>
                  <div
                    style={{
                      color: "#ff5f16",
                      width: "70px",
                      textAlign: "right",
                    }}
                  >
                    <p style={{ marginBottom: "5px" }}>
                      ￥{item.lowPrice / 1000}起
                    </p>
                    <p
                      style={{
                        color: "#797d82",
                        fontSize: "10px",
                        lineHeight: "26px",
                      }}
                    >
                      距离{item.distance}
                    </p>
                  </div>
                </li>
              ))}
            </ul>
          </JumboTabs.Tab>
          <JumboTabs.Tab title="周日9月25日" description="" key="7">
            <ul>
              {cinem.map((item, i) => (
                <li
                  key={item.id}
                  style={{
                    padding: "10px",
                    boxSizing: "border-box",
                    display: "flex",
                    justifyContent: " space-between",
                  }}
                >
                  <div style={{ width: "calc(100vw - 100px)" }}>
                    <p
                      style={{
                        fontSize: "16px",
                        color: "#191a1b",
                        overflow: "hidden",
                        marginBottom: "5px",
                        textOverflow: "ellipsis",
                        whiteSpace: "nowrap",
                      }}
                    >
                      {item.name}
                    </p>
                    <p
                      style={{
                        fontSize: "13px",
                        color: "#797d82",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                        whiteSpace: "nowrap",
                      }}
                    >
                      {item.adress}
                    </p>
                  </div>
                  <div
                    style={{
                      color: "#ff5f16",
                      width: "70px",
                      textAlign: "right",
                    }}
                  >
                    <p style={{ marginBottom: "5px" }}>
                      ￥{item.lowPrice / 1000}起
                    </p>
                    <p
                      style={{
                        color: "#797d82",
                        fontSize: "10px",
                        lineHeight: "26px",
                      }}
                    >
                      距离{item.distance}
                    </p>
                  </div>
                </li>
              ))}
            </ul>
          </JumboTabs.Tab>
        </JumboTabs>
      </div>
    </div>
  );
}
