interface actors_inter{
    name:string,
    role:string,
    avatarAddress:string
}
interface filmType{
    name:string,
    value:string
}
export interface filmsing_inter{
    id?:number,
    name?:string,
    poster?:string,
    actors:actors_inter[],
    director?:string,
    category?:string,
    synopsis?:string,
    filmType:filmType,
    nation?:string,
    runtime?:number,
    [propName: string]: any;

}