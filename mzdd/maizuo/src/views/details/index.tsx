import React, { useState, useEffect } from "react";
import { InHot } from "../films/nowPlaying/type";
import axios from "axios";
import { Ellipsis, Space, Button, Modal } from "antd-mobile";
import { LeftOutline } from "antd-mobile-icons";
// 编程路由
import { useNavigate, NavLink } from "react-router-dom";
// 时间戳转化
import moment from "moment";
// 路由传参
import { useParams } from "react-router-dom";
import style from "./details.module.css";
import "./center-antd.css";
import { URL, inter } from "../../api";
import { ExclamationCircleFill } from "antd-mobile-icons";

export default function Index() {
  const [show, switchShow] = useState(false);
  // 点击折叠收起
  const [content, getcontent] = useState("");
  const [idm, getIdm] = useState("");
  //拿到传过来的id 根据id渲染详情
  let { id } = useParams();
  console.log(id, "id");
  const [details, setDetails] = useState<InHot>({});
  const navgiate = useNavigate();
  // 演员列表
  const [list, setList] = useState<InHot[]>([]);
  // 剧照
  const [photo, setPhoto] = useState([]);
  // const {isphoto,setIsphoto}=useState(false)
  //渲染2D或3D数据
  const [DD, setDD] = useState("");
  const getHot = async () => {
    let res = await axios.get(URL + inter + "films/" + id);
    setDetails(res.data);
    setList(res.data.actors);
    getcontent(res.data.synopsis);
    setDD(res.data.filmType);
    getIdm(res.data.id);
    // 判断数据中是否有剧照
    if (res.data.photos) {
      setPhoto(res.data.photos);
    } else {
      setPhoto([]);
    }
    console.log(res.data.filmType);
  };
  // 点击展开 监听content
  useEffect(() => {
    console.log(content, "123");
  }, [content]);
  useEffect(() => {
    getHot();
    // console.log(details);
  }, []);
  // 点击返回 编程路由
  const click = () => {
    navgiate("/films");
  };
  // 滚动出现顶部
  useEffect(() => {
    const listener = () => {
      switchShow(window.scrollY > 50);
    };
    document.addEventListener("scroll", listener);
    return () => document.removeEventListener("scroll", listener);
  }, []);
  return (
    <div style={{ background: "#f4f4f4" }}>
      <div
        className={style.header}
        style={{ display: show ? "block" : "none", opacity: show ? "1" : "0" }}
      >
        {details.name}
      </div>
      {/* <div style={{ height: "50px", zIndex: "10", lineHeight: "50px" }}></div> */}
      <b className={style.icon}>
        <LeftOutline fontSize={20} onClick={click} />
      </b>
      <div style={{ height: "56vw" }} className={style.Img}>
        <img
          className={style.imgImg}
          style={{ width: "100%" }}
          src={details.poster}
          alt=""
        />
      </div>

      <div className={style.details_body}>
        {/* 名字，评分 */}
        <p>
          <span style={{ fontSize: 20, color: "#191a1b" }}>
            {" "}
            {details.name}
          </span>
          <span className={style.DD}>{DD.name}</span>
          <span style={{ float: "right", color: "#ffb232", fontSize: "16px" }}>
            <i> {details.grade}</i> 分
          </span>
        </p>
        {/* 类型 */}
        <p style={{ fontSize: "14px", color: "#797d82" }}>{details.category}</p>
        {/* 上映时间 */}
        <p style={{ fontSize: "13px", color: "#797d82" }}>
          {moment(details.premiereAt * 1000).format("YYYY")}年上映
        </p>
        {/* 时长 */}
        <p style={{ fontSize: "13px", color: "#797d82" }}>
          {details.nation} | {details.runtime}
        </p>
        {/* 简介 */}
        <div style={{ fontSize: "13px", color: "#797d82" }}>
          {/* <DemoBlock title="展开收起"> */}
          <Ellipsis
            direction="end"
            content={content}
            expandText="展开"
            collapseText="收起"
          />
          {/* </DemoBlock> */}
        </div>
        {/* <div className={style.xiala}>∨</div> */}
      </div>
      {/* 演员列表 */}
      <div className={style.performerList}>演员列表</div>
      <div style={{ background: "#fff" }} className={style.iFilms}>
        <div className={style.films_performer}>
          {list.map((item, i) => (
            <dl key={i}>
              <img src={item.avatarAddress} alt="" />
              <dt>{item.name}</dt>
              <dd>{item.role}</dd>
            </dl>
          ))}
        </div>
      </div>

      {/* 弹窗 */}
      {/* <Button
        block
        onClick={() =>
          Modal.alert({
            content: "人在天边月上明",
            onConfirm: () => {
              console.log("Confirmed");
            },
          })
        }
      >
        最简单的弹窗
      </Button> */}

      {/* 剧照 */}
      <div className={style.photoTil}>
        <span style={{ fontSize: "16px" }}>剧照</span>
        <span
          style={{ color: "#797d82", cursor: "pointer" }}
          onClick={() => {
            Modal.alert({
              header: <div>剧照</div>,

              content: (
                <>
                  <ul className={style.juzhao}>
                    {photo.map((item, i) => (
                      <li className={style.photoImg}>
                        <img src={item} alt="" />
                      </li>
                    ))}
                  </ul>
                </>
              ),
            });
          }}
        >
          全部({photo.length}) ☞
        </span>
      </div>

      {photo.length > 0 ? (
        <div style={{ background: "#fff" }} className={style.iFilms1}>
          <>
            <ul className={style.photoUl}>
              {photo.map((item, i) => (
                <li className={style.photoImg}>
                  <img src={item} alt="" />
                </li>
              ))}
            </ul>
          </>
        </div>
      ) : (
        <div style={{ width: "100%" }}>
          <img
            style={{ width: "100%" }}
            src="https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fp1.itc.cn%2Fq_70%2Fimages03%2F20201020%2F4dae0b16e59743e3901a1436841f860f.jpeg&refer=http%3A%2F%2Fp1.itc.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1665917883&t=9df3151af41e8d760a99b7af7aa0c301"
            alt=""
          />
        </div>
      )}

      <div className={style.movieTicket}>
        <NavLink to={"/playing/" + id}>选座购票</NavLink>
      </div>

      {/* <div className={style.mengceng}>1231312313</div> */}
    </div>
  );
}
