import React, { useState, useEffect } from 'react'
import { URL, inter } from '../../api'
import { NavLink } from 'react-router-dom'
import menu from "../../hoc/menu";
import axios from 'axios'
import './index.scss'
interface IMoive {
  name: string
  poster: string
  [propName: string]: any
}
const Zixun = () => {
  const [moive, setMovie] = useState<IMoive[]>([])
  const getMoive = () => {
    axios.get(URL + inter + 'zixun').then(res => {
      // console.log(res, 'res');
      setMovie(res.data)
    })
  }
  useEffect(() => {
    getMoive()
  }, [])
  return (
    <div>
      {
        moive.map((item) => (
          <NavLink to={'/information/' + item.id} key={item.id} >
            <dl className='dl'>
              <dt>
                <p>{item.name}</p>
                <p className='plot'>{item.category}</p>
                <p className='yanyuan'>{item.director}</p>
              </dt>
              <dd>
                <img src={item.poster} alt="" />
              </dd>
            </dl>
          </NavLink>
        ))
      }
    </div >
  )
}
export default menu(Zixun)