import React, { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import { URL, inter } from '../../../api'
import { Ellipsis, Space } from "antd-mobile";
import { LeftOutline } from "antd-mobile-icons";
import axios from 'axios'
import './index.scss'
import moment from "moment";
interface IDetails {
    [propName: string]: any
}
export default function Information() {
    const navgiate = useNavigate()
    let { id } = useParams()
    // 简介
    const [content, getcontent] = useState("");
    const [information, setInformation] = useState<IDetails>([])
    // 演员
    const [performer, setPerformer] = useState<IDetails>([])
    // 电影类型
    const [D, setD] = useState<IDetails>([])
    const getInfoormation = async () => {
        let res = await axios.get(URL + inter + 'zixun/' + id)
        // console.log(res);
        setInformation(res.data)
        setPerformer(res.data.actors)
        setD(res.data.item)
        getcontent(res.data.synopsis);
    }
    // 点击展开 监听content
    useEffect(() => {

    }, [content]);
    useEffect(() => {
        getInfoormation()
    }, [])
    // 点击返回资讯
    const click = () => {
        navgiate("/zixun");
    };
    return (
        <div>
            <div className='inTop'>
                {/* 电影图片 */}
                <img src={information.poster} alt="" />
                <b className='inicon'>
                    <LeftOutline fontSize={20} onClick={click} />
                </b>
                {/* 电影类型 */}
                <div className='intype'>
                    <p ><b>{information.name}</b> <i>{D.name}</i> <span>{information.grade}分</span></p>
                    {/* 上映信息 */}
                    <ul>
                        <li>{information.category}</li>
                        <li>{moment(information.premiereAt * 1000).format("YYYY")}年上映</li>
                        <li>{information.nation} | {information.runtime}分钟</li>
                        <Ellipsis
                            direction="end"
                            content={content}
                            expandText="展开"
                            collapseText="收起"
                        />
                    </ul>
                </div>
            </div>
            {/* 演员列表 */}
            <div className='inperformer'>演员列表</div>
            <div className='inperformer1'>
                <div className='infilms_performer'>
                    {performer.map((item, i) => (
                        <dl key={i} >
                            <img src={item.avatarAddress} alt="" />
                            <dt>{item.name}</dt>
                            <dd>{item.role}</dd>
                        </dl>
                    ))}
                </div>
            </div>
        </div>
    )
}
