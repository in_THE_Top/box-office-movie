//定义高阶组件在hoc，高阶组件是由高阶函数生成得，本质是个函数
//高阶组件不是非用不可，很多组件都需要统一的逻辑，则可以将这部分逻辑提出来设置为高阶组件，方便维护

//比如这个导航menu有的需要，有的可以没有
import React from "react";
import Menu from "../components/menu";
/**
 *
 *   用它就有导航 不是用它就不显示导航
 */

    //如果我还想在其他地方拿到这里的数据，那就再Com上传{...props},代表了传递props的所有属性，这个很重要，不然数据传不通，到这里就阻断了
function menu(Com) {
  function Hoc(props) {
    return (
      <>
        <Com {...props}></Com>
        <Menu />
      </>
    );
  }
  return Hoc;
}

export default menu;
