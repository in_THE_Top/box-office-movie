import Center from "../views/center";
import Cinemas from "../views/cinemas";
import C_Detail from "../views/cinemas/detail";
import Films from "../views/films";
import Zixun from "../views/zixun";
import NowPlaying from "../views/films/nowPlaying";
import ComingSoon from "../views/films/comingSoon";
import Login from "../views/login";
import Details from "../views/details";
import Playing from "../views/playing";
import Search from "../views/search";
import Information from "../views/zixun/information";
import SoonDetails from "../views/films/comingSoon/soon_details";
import SoonShop from "../views/films/comingSoon/SoonShop";
import No from "../views/404";
//订单路由
import Dingdan from '../views/center/dingdan'
import RedBag from '../views/center/redBag'
import Help from '../views/center/help'
import SheZhi from '../views/center/shezhi'
import {
  MovieOutline,
  FileOutline,
  UserOutline,
  GlobalOutline,
  CollectMoneyOutline,
} from "antd-mobile-icons";

export const routes = [
  {
    path: "/search",
    element: Search,
    til: "搜索",
    hidden: true,
    icon: GlobalOutline,
  },
  {
    path: "/soondetails/:id",
    element: SoonDetails,
    til: "搜索",
    hidden: true,
    icon: GlobalOutline,
  },
  {
    path: "/soonshop/:id",
    element: SoonShop,
    til: "搜索",
    hidden: true,
    icon: GlobalOutline,
  },
  {
    path: "/playing/:id",
    element: Playing,
    til: "购票",
    hidden: true,
    icon: GlobalOutline,
  },
  {
    path: "/details/:id",
    element: Details,
    til: "详情页",
    hidden: true,
    icon: GlobalOutline,
  },
  {
    path: "/films",
    element: Films,
    til: "电影",
    hidden: false,
    icon: MovieOutline,
    children: [
      {
        path: "",
        element: NowPlaying,
        hidden: false,
        til: "正在热映",
      },
      {
        path: "comingSoon",
        element: ComingSoon,
        hidden: false,
        til: "即将上映",
      },
    ],
  },
  {
    path: "/cinemas",
    element: Cinemas,
    til: "影院",
    hidden: false,
    icon: GlobalOutline,
  },
  {
    path: "/cinemas/detail/:id",
    element: C_Detail,
    til: "影院详情",
    hidden: true,
    icon: GlobalOutline,
  },
  {
    path: "/zixun",
    element: Zixun,
    til: "资讯",
    hidden: false,
    icon: FileOutline,
  },
  {
    path: "/information/:id",
    element: Information,
    hidden: true,
    til: "资讯详情",
  },
  {
    path: "/center",
    element: Center,
    til: "我的",
    hidden: false,
    icon: UserOutline,
    children: [
      {
        path: "dingdan",
        element: Dingdan,
        til: "电影订单",
        hidden: true,
        icon: CollectMoneyOutline,
      },
      {
        path: "redBag",
        element: RedBag,
        til: "组合红包",
        hidden: true,
        icon: CollectMoneyOutline,
      },
      {
        path: "help",
        element: Help,
        til: "帮助与客服",
        hidden: true,
        icon: CollectMoneyOutline,
      },
      {
        path: "shezhi",
        element: SheZhi,
        til: "设置",
        hidden: true,
        icon: CollectMoneyOutline,
      },
    ],
  },
  {
    path: "/",
    element: Login,
    til: "登陆",
    hidden: true,
  },
  {
    path: "*",
    element: No,
    til: "404",
    hidden: true,
    icon: FileOutline,
  },
];
